        program MandelBench_Fortran
	        implicit none
			
	        integer*4 :: its, maxIts
			
	        integer*4 :: pix_real = 0, pix_imag = 0, iteration = 0, dwell = 0
            integer, parameter :: maxIterations = 1000000 
	        double precision :: z_real = 0.0, z_imag = 0.0, w_real = 0.0, w_imag = 0.0, c_real = 0.0, c_imag = 0.0
		    double precision :: timeSnap1 = 0.0, timeSnap2 = 0.0
			!-- What's below corresponds to the long ints in Java
	        integer*4 :: totalIterationCount = 0
			
			
	        character(80) :: line
            logical :: bailout = .false.

	        logical :: flag = .false.
            integer :: tempint = -1
			
		    integer*4 :: date_time(8)
	    	character*10 :: b(3)
	    	
		    Character*10 :: str
			
			!-- and now, the procedure
            
		    call date_and_time(b(1), b(2), b(3), date_time)
	    	print *,'date_time    array values:'
	    	print *,'year=',date_time(1)
	    	print *,'month_of_year=',date_time(2)
	    	print *,'day_of_month=',date_time(3)
	    	print *,'time difference in minutes=',date_time(4)
	    	print *,'hour of day=',date_time(5)
	    	print *,'minutes of hour=',date_time(6)
	    	print *,'seconds of minute=',date_time(7)
	    	print *,'milliseconds of second=',date_time(8)
	    	print *, 'DATE=',b(1)
	    	print *, 'TIME=',b(2)
	    	read( b(2), '(D6.3)' ) timeSnap1
		    write (*,'(D6.3)') timeSnap1
		    print *, 'ZONE=',b(3)
        
	    
		    print *, "Hello World!"
		    call date_and_time(b(1), b(2), b(3), date_time)
	        str = b(2)
		    read( str, '(D6.3)' ) timeSnap1
			
			do pix_imag = 19, -20, -1
    	        c_imag = pix_imag / 20.00000000000000000000000
    			!c_imag = 5 + pix_imag / 20.0
    			
				line = ""
    			do pix_real = -40, 39, 1
    			    bailout = .false.
    			    c_real = pix_real / 40.000000000000000000000 - .5000000000000000000000
    			    z_real = 0.0
    			    z_imag = 0.0
    			    iteration = 0
				    do iteration = 0, maxIterations-1, 1 ! remember to change this do a do while loop.
    				!for (iteration = 0; ((iteration < maxIterations) && !bailout); iteration++) {
    					w_real = z_real**2 - z_imag**2 + c_real
    					w_imag = (2.0 * z_real * z_imag) + c_imag
        				z_real = w_real
        				z_imag = w_imag
        				if (z_real**2 + z_imag**2 .ge. 4.0) bailout = .true.
	        			if (z_real > 1.0) bailout = .true.
						
						!if (2+2 == 4) bailout = .true.
						
						if (bailout) exit
        				
        				!exit
						totalIterationCount = totalIterationCount + 1
	        			
    				end do 
    				!}
		
    				!//line = line + computeCharacter(iteration);
    				!write (*,'(A)',advance='no') computeCharacter(iteration)			
				    call capCharacter(iteration, maxIterations)
	    			!call printStatus(z_real, z_imag, iteration)
					!System.out.print(computeCharacter(iteration));
                end do
    			!}
				
                !call logicTest(.false.)
			    !call logicTest(.true.)
    			
				!//System.out.println(line);
    			write (*,'(A)') ""
    			!System.out.println("  ");
				!write (*,*) "=("
	    		!//System.out.println("=(");
    		end do
    		!}
    		
	
		    call date_and_time(b(1), b(2), b(3), date_time)
	        str = b(2)
		    read( str, '(D6.3)' ) timeSnap2
			
			
    		write (*,'(A)',advance='no') "Total iterations calculated: "
    		write (*,*) totalIterationCount
    		!System.out.println("Total iterations calculated: " + totalIterationCount);
	    	
		    write (*,'(A)', advance='no') "Elapsed time: "
        
    		write (*,'(D6.3)', advance='no') (timeSnap2 - timeSnap1)
			
		    write (*,'(A)') " seconds."
			
		    print *, 'timeSnap1 = ',timeSnap1
		    print *, 'timeSnap2 = ',timeSnap2
			
	    	
	   	    end program MandelBench_Fortran
	
        subroutine capCharacter(its, maxIts)
		    integer, intent(in) :: its, maxIts
		    !integer :: iteration, maxIterations
		   
            if (its .le. 2) then
			    write (*,'(A)',advance="no") 'M'
		    end if
			!if (iteration == 5) write (*,'(A)',advance="no") '0'                M
    		if ((its  >=.  3) .and. (its <=  5)) write (*,'(A)',advance="no") 'X'   	
    		if ((its .ge.  6) .and. (its .le.  8)) write (*,'(A)',advance="no") '#'   	
    		if ((its .ge.  9) .and. (its .le. 11)) write (*,'(A)',advance="no") 'B'  	
    		
			if ((its .ge. 12) .and. (its .le. 15)) write (*,'(A)',advance="no") 'F'  	
    		if ((its .ge. 16) .and. (its .le. 19)) write (*,'(A)',advance="no") 'S'   	
    		if ((its .ge. 20) .and. (its .le. 23)) write (*,'(A)',advance="no") '@'   	
    		
			if ((its .ge. 24) .and. (its .le. 28)) write (*,'(A)',advance="no") 'Q'   	
    		if ((its .ge. 29) .and. (its .le. 33)) write (*,'(A)',advance="no") 'O'   	
    		if ((its .ge. 34) .and. (its .le. 38)) write (*,'(A)',advance="no") '0'   	
    		
			if ((its .ge. 39) .and. (its .le. 44)) write (*,'(A)',advance="no") 'o'   	
    		if ((its .ge. 45) .and. (its .le. 50)) write (*,'(A)',advance="no") 'x'  	
    		if ((its .ge. 51) .and. (its .le. 56)) write (*,'(A)',advance="no") '+'  	
    		
			if ((its .ge. 57) .and. (its .le. 63)) write (*,'(A)',advance="no") '-'   	
    		if ((its .ge. 64) .and. (its .le. 70)) write (*,'(A)',advance="no") '~'   	
    		if ((its .ge. 71) .and. (its .le. 77)) write (*,'(A)',advance="no") ','   	
    		
			if ((its .ge. 78) .and. (its .le. 85)) write (*,'(A)',advance="no") '.'
    		if ((its .ge. 86) .and. (its .le. 93)) write (*,'(A)',advance="no") '`'
			
    		
			
			
			if ((its .ge. 94)) write (*,'(A)',advance="no") ' '   	
    		
    	 end subroutine capCharacter
 
!        subroutine capCharacter(its, maxIts)
!		    integer, intent(in) :: its, maxIts
!		    integer :: iteration, maxIterations
!		   
!            if (iteration .le. maxIterations/4.0) then
!			    write (*,'(A)',advance="no") '0'
!		    end if
!			!if (iteration == 5) write (*,'(A)',advance="no") '0'
!    		if ((maxIterations/4.0 <= iteration) .and. (iteration < maxIterations/2.0)) write (*,'(A)',advance="no") 'o'   	
!    		if ((maxIterations/2.0 <= iteration) .and. (iteration < 3.0*maxIterations/4.0)) write (*,'(A)',advance="no") '.'    	
!    		if (iteration >= 3.0*maxIterations/4.0) write (*,'(A)',advance="no") ' '    	
!    		!write (*,'(A)',advance="no") 'X'
!			!write (*,'(A)',advance="no") ':'
!			!write (*,'(A)',advance="no") '('
!    	 end subroutine capCharacter
 
 
        subroutine printStatus(z_real, z_imag, iteration)
		    double precision, intent(in) :: z_real, z_imag
	        integer*4, intent(in) :: iteration
			
		    write (*,'(A)',advance="no") 'iteration is: '
            write (*,'(I15)') iteration
            
		    
		    write (*, '(D1.9)') z_real
	        write (*, '(A)',advance="no") ' '
		    write (*, '(D1.9)',advance="no") z_imag
        end subroutine printStatus
			
        subroutine logicTest(flag)
		    logical, intent(in) :: flag
			
			if (flag) write (*,'(A)') "flag is .true.."
		    if (.not.flag) write (*,'(A)') "flag is .false.."
        end subroutine logicTest